# dump-tiff-data.py
#
# Michael Riffle <mriffle@uw.edu>
# November 2018
# this script reads in a tiff and outputs:
#    - all pixel values (.pixels.txt)
#    - histogram (.histogram.txt)
#    - stats (.stats.txt)
#
# this script assumes that it is a grayscale tiff
#
# dependency:
# pip3 install Pillow
# pip3 install numpy
#
from PIL import Image
import numpy
import sys
import os.path
import glob

def process_tiff_file( tiff_file ):

    image = Image.open( tiff_file)

    pixel_values = list(image.getdata())
    pixel_value_counts = { }

    # write out all pixel values
    file = open( tiff_file + ".pixels.txt","w") 

    for pixel_value in pixel_values:
        file.write( str(pixel_value) + "\n" )

        if not pixel_value in pixel_value_counts:
            pixel_value_counts[ pixel_value ] = 1
        else:
            pixel_value_counts[ pixel_value ] = pixel_value_counts[ pixel_value ] + 1

    file.close() 

    # write out histogram data

    background_intensity = 0
    background_intensity_count = 0

    file = open( tiff_file + ".histogram.txt","w") 

    for key in range(0, max(pixel_values) + 1):
        if key not in pixel_value_counts:
            file.write( str(0) + "\n" )
        else:
            file.write( str(pixel_value_counts[key]) + "\n" )

            if pixel_value_counts[key] > background_intensity_count:
                background_intensity_count = pixel_value_counts[key]
                background_intensity = key

    file.close() 

    # get sum of all pixel intensities minus calculated background

    pixel_intensity_sum = 0

    for pixel_value in pixel_values:
        pixel_intensity_sum = pixel_intensity_sum + ( pixel_value - background_intensity )

    # write out very simple image stats
    file = open( tiff_file + ".stats.txt","w") 

    file.write( "max:\t" + str( max( pixel_values ) ) + "\n" )
    file.write( "min:\t" + str( min( pixel_values ) ) + "\n" )
    file.write( "mean:\t" + str( numpy.mean( pixel_values ) ) + "\n" )
    file.write( "background:\t" + str( background_intensity ) + "\n" )
    file.write( "total signal:\t" + str(pixel_intensity_sum ) + "\n" )

    file.close()

def process_directory( directory ):
    for tiff_file in glob.glob( directory + "*.tif*"):
        print( "Processing file: " + tiff_file )
        process_tiff_file( tiff_file )



request_file = sys.argv[ 1 ]

if os.path.isdir( request_file ):
    process_directory( request_file )
elif os.path.isfile( request_file ):
    process_tiff_file( request_file )
else:
    print( "Could not find file: " + request_file )
    sys.exit( 1 )
